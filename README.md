# dsr-ci-demo

CI/CD demo for DSR

A barebones Node.js app using [Express 4](http://expressjs.com/).

This application is taken from the Heroku [Getting Started with Node on Heroku](https://devcenter.heroku.com/articles/getting-started-with-nodejs) article.
